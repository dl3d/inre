{-# LANGUAGE OverloadedStrings #-}















-- sass
sassCompiler :: Compiler (Item String)
sassCompiler =
	getResourceString
		>>= withItemBody (unixFilter "sass" ["-s", "-scss"]) -- "--sitemap"
		>>= return . fmap compressCss