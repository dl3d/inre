Doc/manual-esque blogging experiment with cabal support for R-pandoc-* objects, graphics-directed pre-processing, ad hoc reporting routes
---

### Cabal + Hakyll setup  

Initialize repo with the default hakyll template, managing dependencies with sandbox:    

```
hakyll-init inre
cd inre
git init
```

Adjust the `build-depends`  and revised default [*.cabal](https://github.com/dled/inre/blob/master/inre.cabal). Then cabalizing:  

```
cabal sandbox init
cabal install --only-dependencies
cabal install
```

### Creditworthy and far better examples:  
[Austin Rochford](http://austinrochford.com/)  
[Blaenk Denum](http://www.blaenkdenum.com/) general [switch to Hakyll](http://www.blaenkdenum.com/posts/the-switch-to-hakyll/)  
[Chromatic Leaves](http://chromaticleaves.com/) in re: [cabal workflow](http://chromaticleaves.com/posts/cabal-sandbox-workflow.html)  
[Corentin Dupont](http://www.corentindupont.info/index.html) on [R-pandoc](http://www.corentindupont.info/blog/posts/Programming/2015-09-14-diagrams.html)  
[Gísli Kristjánsson](http://gisli.hamstur.is/)  
[Martin Hilscher](https://xinitrc.de/)  
[Travis Brown](https://meta.plasm.us/hakyll)  


### Other considerations  

#### Sass + Compass
``` 
gem install sass compass
cabal update
cabal sandbox init
cabal install --only-dependencies
ghc --make -O2 -Wall -threaded Init.hs
```

[Flag reference](https://downloads.haskell.org/~ghc/7.8.2/docs/html/users_guide/flag-reference.html) for `ghc --make`  
- `Wall` enables (all) (W)arnings
- `O2` [optimize](https://downloads.haskell.org/~ghc/7.8.2/docs/html/users_guide/options-optimise.html) _n_  levels
- `threaded` runtime  