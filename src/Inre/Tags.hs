{-# LANGUAGE OverloadedStrings #-}
module Inre.Tags where
import Control.Applicative	((<$>))
import Control.Monad	(forM)
import Data.List		(intercalate, intersperse)
import Data.Maybe		(catMaybes)
import Data.Monoid		(mconcat)

import Hakyll
import Text.Blaze.Html	(toHtml, toValue, (!))
import Text.Blaze.Html.Renderer.String (renderHtml)

import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

-- see travisbrown's metaplasm/src/Metaplasm/Tags.hs
renderTagList :: Tags -> Compiler String
renderTagList = renderTags
