--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

import Hakyll

import Control.Applicative  ((<$>)) 

import qualified Data.Set as S
import qualified Data.Map as M
import Data.List (intercalate, intersperse)

-- System modules
--import System.Environment
--import System.FilePath (takeFileName)

import           Text.Pandoc.Options


--------------------------------------------------------------------------------
-- Configuration (in Inre.Config)
--------------------------------------------------------------------------------
--- TO DO
-- 1. site map
-- 2. atom feed
-- 3. tag pattern
-- 4. scss dependencies
-- 5. month buckets (archives)

--------------------------------------------------------------------------------
-- Rules & rendering
--------------------------------------------------------------------------------
siteConf :: SiteConfiguration
siteConf = SiteConfiguration
    { siteRoot = "https://inre.io"
    , siteGaId = "UA-70253618-1" }

feedConf :: FeedConfiguration
feedConf = FeedConfiguration {
  feedTitle = "In regress:"
 ,feedDescription = "Graphics-directed stats blog"
 ,feedAuthorName = "Dylan Ledbetter"
 ,feedAuthorEmail = "dtledbetter@gmail.com"
 ,feedRoot = "http://www.inre.io"
 }