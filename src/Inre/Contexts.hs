{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Inre.Contexts (
  siteCtx,
  postCtx,
  tagsCtx
) where

import Hakyll hiding (titleField)

siteCtx :: Context String
siteCtx =
	bodyField		"body"						<>
	metadataField								<>
	titleField									<>
	niceUrlField	"url"						<>
	pathField		"path"						<>
	constField		"root"	(siteRoot siteConf)	<>
	constField		"gaId"	(siteGaId siteConf)	<>
	constField		"title"	"Infinite Regress"	<>
	missingField

postCtx :: Context String
postCtx =
	dateField	"date"		"%e %B %Y"	<>
	datetime	"datetime"	"%Y-%m-%d"	<>
	gitTag		"git"					<>
	siteCtx

tagsCtx :: Tags -> Context String
tagsCtx tags = tagsField "tags" tags <> postCtx